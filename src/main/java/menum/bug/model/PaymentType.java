package menum.bug.model;

public enum PaymentType {
  CARD,
  TRANSFER,
  CASH,
  BON;
}

