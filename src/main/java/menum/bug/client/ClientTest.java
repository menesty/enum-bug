package menum.bug.client;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.DefaultHttpClient;
import io.micronaut.http.client.multipart.MultipartBody;

import java.net.MalformedURLException;
import java.net.URL;

public class ClientTest {
  public static void main(String... arg) throws MalformedURLException {
    try (DefaultHttpClient httpClient = new DefaultHttpClient(new URL("http://localhost:8080"))) {

      MultipartBody requestBody = MultipartBody.builder()
              .addPart("paymentType", (String) null)
              .build();

      HttpRequest<MultipartBody> request = HttpRequest.POST("/api/data", requestBody).contentType(MediaType.MULTIPART_FORM_DATA_TYPE);

      String result = httpClient.exchange(request, String.class)
              .blockingSingle()
              .getBody(String.class).orElseThrow();
    }

  }
}
