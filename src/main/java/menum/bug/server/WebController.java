package menum.bug.server;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.multipart.CompletedFileUpload;
import io.reactivex.Single;
import menum.bug.model.TestServerEntity;

@Controller("/")
public class WebController {

  @Post(value = "/api/data", consumes = MediaType.MULTIPART_FORM_DATA)
  public Single<String> create(TestServerEntity entity, CompletedFileUpload file) {


    return Single.just("test");
  }
}
